package nivelamentoestruturacondicional;

import java.util.Scanner;

public class Exercicio05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("digite o nome");
        String nome = scanner.nextLine();

        System.out.println("digite o valor da primeira nota");
        Double a = scanner.nextDouble();

        System.out.println("digite o valor da segunda nota");
        Double b = scanner.nextDouble();

        System.out.println("digite o valor da terceira nota");
        Double c = scanner.nextDouble();

        double v = (a + b + c) / 3;

        if ( v > 6){
            System.out.println(nome + " aprovado, sua media foi " + v);
        } else if ( v < 6)
            System.out.println(nome + " reprovado, sua media foi " + v);{

        }

    }
}
