package nivelamentoestruturacondicional;

import java.util.Scanner;

public class Exercicio06 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("digite para saber qual o maior e o menor");
        Double n1 = scanner.nextDouble();

        System.out.println(" digite o segundo numero");
        Double n2 = scanner.nextDouble();

        if (n1 > n2){
            System.out.println(n1 + " é maior que " + n2);
        }else
            System.out.println(n2 + " é maior que " + n1);

    }
}
